
// first name
var text = document.getElementById("reg_first_name_check").value.replace (/ +/g, " ");
 const fname = document.querySelector('#reg_first_name_check');
    fname.addEventListener('input', () => {
      fname.value = fname.value ? fname.value.trimStart() : ''; 
    })

$(function() {
  $("input[name='reg_first_name']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      // $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
});

function val_first_name(){
	var reg_name_s = document.getElementById("reg_first_name_check").value;
	var name_pattern = /^[^\s].+[A-Za-z\s]+[^\s]$/;
	if(name_pattern.test(reg_name_s) == true){	
		document.getElementById("enter_first_name").innerHTML = "" ;
		document.getElementById('val_submit').disabled=false;
		return true;

	}
	else if (name_pattern.test(reg_name_s) == false){
		document.getElementById("enter_first_name").innerHTML = "enter valid name" ;
		document.getElementById('val_submit').disabled=true;
		return false;
	}

}
// last name
var text = document.getElementById("reg_last_name_check").value.replace (/ +/g, " ");
 const lname = document.querySelector('#reg_last_name_check');
    lname.addEventListener('input', () => {
      lname.value = lname.value ? lname.value.trimStart() : ''; 
    })

$(function() {
  $("input[name='reg_last_name']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
});

function val_last_name(){
	var reg_name_s = document.getElementById("reg_last_name_check").value;
	var name_pattern = /^[^\s].+[A-Za-z\s]+[^\s]$/;
	if(name_pattern.test(reg_name_s) == true){	
		document.getElementById("enter_last_name").innerHTML = "" ;
		document.getElementById('val_submit').disabled=false;
		return true;

	}
	else if (name_pattern.test(reg_name_s) == false){
		document.getElementById("enter_last_name").innerHTML = "enter valid name" ;
		document.getElementById('val_submit').disabled=true;
		return false;
	}

}
//email
const space_rem_email = document.querySelector('#reg_email_check');
    space_rem_email.addEventListener('input', () => {
      space_rem_email.value = space_rem_email.value ? space_rem_email.value.trimStart() : ''; 
    })
   
function val_email(){
	var email_err;
	var reg_email_s = document.getElementById("reg_email_check").value;
	var email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
	if(email_pattern.test(reg_email_s) == false){	
		document.getElementById("enter_email").innerHTML = "not valid";
		document.getElementById('val_submit').disabled=true	;
		// return true;
		email_err=false;
	}
	else {
		document.getElementById("enter_email").innerHTML = "";
		document.getElementById('val_submit').disabled=false;
		// return false;
		email_err=true;
	}
	return email_err;
}

function val_mobile(){
	var reg_mobile_s = document.getElementById("reg_mob_check").value;
	var mob_pattern = /^[9876][0-9]{9}/;
	if (mob_pattern.test(reg_mobile_s) == true) {
		document.getElementById("enter_mob").innerHTML = "";
		document.getElementById('val_submit').disabled=false;
		return true;
	}
	else {
		document.getElementById("enter_mob").innerHTML = "enter valid Mobile no." ;
		document.getElementById('val_submit').disabled=true;
		return false;
	}
}

const space_rem_pass = document.querySelector('#reg_pass_check');
    space_rem_pass.addEventListener('input', () => {
      space_rem_pass.value = space_rem_pass.value ? space_rem_pass.value.trimStart() : ''; 
    })

function val_pass(){
	var reg_pass_s = document.getElementById("reg_pass_check").value;
	var reg_conf_pass_check = document.getElementById("reg_conf_pass_check").value;
	var upp=/(?=.*[A-Z])/;
	var low=/(?=.*[a-z])/;
	var digit=/(?=.*\d)/;
	var sp=/(?=.*[@#$!^%*?&])/;
	var pass_pattern = /^(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!^%*?&]{8,}$/;
	if(upp.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one uppercase";
		document.getElementById('val_submit').disabled=false;
		return false;
	}
	else if(low.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one lower";
		document.getElementById('val_submit').disabled=false;
		return false;
	}
	else if(digit.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one digit";
		document.getElementById('val_submit').disabled=false;
		return false;
	}else if(sp.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one spec";
		document.getElementById('val_submit').disabled=false;
		return false;
	}else if(pass_pattern.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast 8 cracters";
		document.getElementById('val_submit').disabled=false;
		return false;
	}
	else{
		document.getElementById("enter_pass").innerHTML = "";
		return true;
	}
}

const space_rem_pass_conf = document.querySelector('#reg_conf_pass_check');
    space_rem_pass_conf.addEventListener('input', () => {
      space_rem_pass_conf.value = space_rem_pass_conf.value ? space_rem_pass_conf.value.trimStart() : ''; 
    })

function val_conf_pass(){
	var reg_pass = document.getElementById("reg_pass_check").value;
	var reg_conf_pass_check = document.getElementById("reg_conf_pass_check").value;
	if (reg_pass != reg_conf_pass_check) {
		document.getElementById("reg_pass_check").style.border = "";
		document.getElementById('val_submit').disabled=true;
		return false;
	}
	else {
		document.getElementById("reg_pass_check").style.border = "none";
		document.getElementById("reg_pass_check").style.borderBottom = "2px solid #0174b9";
		document.getElementById('val_submit').disabled=false;
		return true;
	}
}
function validatePassword(){
	var reg_pass = document.getElementById("reg_pass_check").value;
	var reg_conf_pass_check = document.getElementById("reg_conf_pass_check").value;
	if (reg_pass == reg_conf_pass_check || reg_conf_pass_check == "") {
		document.getElementById("enter_conf_pass").innerHTML = "";
		document.getElementById('val_submit').disabled=false;
		return false
	}
	else {
		document.getElementById("enter_conf_pass").innerHTML = "password does not match";
		document.getElementById('val_submit').disabled=true;
		return true;
	}
}


const space_rem_addr = document.querySelector('#reg_addr_check');
    space_rem_addr.addEventListener('input', () => {
      space_rem_addr.value = space_rem_addr.value ? space_rem_addr.value.trimStart() : ''; 
    })
    
$(function() {
  $("input[name='addr']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      // $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
});
function val_addr(){
	var reg_addr_s = document.getElementById("reg_addr_check").value;
	var pattern_addr = /^\w[#.0-9a-zA-Z\s,-/]+$/;
	if (pattern_addr.test(reg_addr_s) == true) {
		document.getElementById('enter_addr').innerHTML= "";
		document.getElementById('val_submit').disabled=false;
		return true;
	}
	else{
		document.getElementById("enter_addr").innerHTML="Please Enter address"; 
		document.getElementById('val_submit').disabled=true;
		return false;
	}
}

function val_pin(){
	var reg_pin_s = document.getElementById("reg_pin_check").value;
	var pattern_pin = /^[1-9].[0-9]{4,6}/;
	if (pattern_pin.test(reg_pin_s) == true) {
		document.getElementById('enter_pin').innerHTML= "";
		document.getElementById('val_submit').disabled=false;
		return true;
	}
	else{
		// document.getElementById("enter_pin").innerHTML="Please Enter Valid Pin"; 
		document.getElementById('val_submit').disabled=true;
		return false;
	}
}

const space_rem_city = document.querySelector('#reg_city_check');
    space_rem_city.addEventListener('input', () => {
      space_rem_city.value = space_rem_city.value ? space_rem_city.value.trimStart() : ''; 
    })

function val_city(){
	var reg_city_s = document.getElementById("reg_city_check").value;
	var pattern_city = /^[^\s]+[A-Za-z\s]+[^\s]$/;
	if (pattern_city.test(reg_city_s) == true) {
		document.getElementById('enter_city').innerHTML= "";
		document.getElementById('val_submit').disabled=false;
		return true;
	}
	else{
		document.getElementById("enter_city").innerHTML="use alphabets only";
		document.getElementById('val_submit').disabled =true;
		return false;
	}
}
const space_rem_state = document.querySelector('#reg_state_check');
    space_rem_state.addEventListener('input', () => {
      space_rem_state.value = space_rem_state.value ? space_rem_state.value.trimStart() : ''; 
    })
function val_state(){
	var reg_state_s = document.getElementById("reg_state_check").value;
	var pattern_state = /^[^\s]+[A-Za-z\s]+[^\s]$/;
	if (pattern_state.test(reg_state_s) == true) {
		document.getElementById('enter_state').innerHTML = "";
		document.getElementById('val_submit').disabled=false;
		return true;
	}
	else{
		document.getElementById("enter_state").innerHTML="use alphabets only";
		document.getElementById('val_submit').disabled=true;
		return false;
	}
}
const space_rem_country = document.querySelector('#reg_country_check');
    space_rem_country.addEventListener('input', () => {
      space_rem_country.value = space_rem_country.value ? space_rem_country.value.trimStart() : ''; 
    })
function val_country(){
	var reg_country_s = document.getElementById("reg_country_check").value;
	var pattern_country = /^[^\s]+[A-Za-z\s]+[^\s]$/;
	if (pattern_country.test(reg_country_s) == true) {
		document.getElementById('enter_country').innerHTML = "";
		document.getElementById('val_submit').disabled=false;
		return true;
	}
	else{
		document.getElementById("enter_country").innerHTML="Please Enter alphabets only";
		document.getElementById('val_submit').disabled=true;
		return false;
	}
}

const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#reg_pass_check");

        togglePassword.addEventListener("click", function () {
            // toggle the type attribute
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            
            // toggle the icon
            this.classList.toggle("fa-eye-slash");
        });
function get_pin(){

  var pincode = document.getElementById("reg_pin_check").value;
  console.log("data");
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `https://api.postalpincode.in/pincode/${pincode}`);
  xhttp.responseType = 'json';
  xhttp.onreadystatechange = function() {
    let data = xhttp.response;
    if (this.readyState == 4 && this.status == 200) {
      console.log("data4",data[0] );
      if(data[0].Status=="Success"){
        console.log('status',data[0].Status);
        var city = data[0].PostOffice[0].District;
        var country = data[0].PostOffice[0].Country;
        var state = data[0].PostOffice[0].State;
        console.log("true");
        document.getElementById("reg_city_check").value = city;
        document.getElementById("reg_country_check").value = country;
        document.getElementById("reg_state_check").value = state;
        document.getElementById('val_submit').disabled=false;
      }else if(data[0].Status=="Error"){
        document.getElementById('enter_pin').innerHTML="*please enter valid pin!!";
        document.getElementById("reg_city_check").value = "";
        document.getElementById("reg_country_check").value = "";
        document.getElementById("reg_state_check").value = "";
        document.getElementById('val_submit').disabled=true;
        
      }
   
    } 
}
  xhttp.send();  



}
