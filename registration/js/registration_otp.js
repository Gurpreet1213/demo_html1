var text = document.getElementById("reg_name_check").value.replace (/ +/g, " ");
 const name = document.querySelector('#reg_name_check');
    name.addEventListener('input', () => {
      name.value = name.value ? name.value.trimStart() : ''; 
    })

const input = document.querySelector('input');
input.addEventListener('keyup', () => {
  input.value = input.value.replace(/  +/g, ' ');
});
const space_rem_name = document.querySelector('#reg_name_check');
    space_rem_name.addEventListener('input', () => {
      space_rem_name.value = space_rem_name.value ? space_rem_name.value.trimStart() : ''; 
    })

function val_name(){
	var reg_name_s = document.getElementById("reg_name_check").value;
	var name_pattern = /^[^\s].+[A-Za-z\s]+[^\s]$/;
	if(name_pattern.test(reg_name_s) == true){	
		document.getElementById("enter_name").innerHTML = "" ;
		// document.getElementById('val_submit').disabled=false;
		return true;

	}
	else if (name_pattern.test(reg_name_s) == false){
		document.getElementById("enter_name").innerHTML = "enter valid name" ;
		// document.getElementById('val_submit').disabled=true;
		return false;
	}

}
const space_rem_email = document.querySelector('#reg_email_check');
    space_rem_email.addEventListener('input', () => {
      space_rem_email.value = space_rem_email.value ? space_rem_email.value.trimStart() : ''; 
    })

function val_email(){
	var email_err;
	var reg_email_s = document.getElementById("reg_email_check").value;
	var email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
	if(email_pattern.test(reg_email_s) == false){	
		document.getElementById("enter_email").innerHTML = "not valid";
		// document.getElementById('val_submit').disabled=true	;
		// return true;
		email_err=false;
	}
	else {
		document.getElementById("enter_email").innerHTML = "";
		// document.getElementById('val_submit').disabled=false;
		// return false;
		email_err=true;
	}
	return email_err;
}
function val_mobile(){
	var reg_mobile_s = document.getElementById("reg_mob_check").value;
	var mob_pattern = /^[9876][0-9]{9}/;
	if (mob_pattern.test(reg_mobile_s) == true) {
		document.getElementById("enter_mob").innerHTML = "";
		// document.getElementById('val_submit').disabled=false;
		return true;
	}
	else {
		document.getElementById("enter_mob").innerHTML = "enter valid Mobile no." ;
		// document.getElementById('val_submit').disabled=true;
		return false;
	}
}

const space_rem_pass = document.querySelector('#reg_pass_check');
    space_rem_pass.addEventListener('input', () => {
      space_rem_pass.value = space_rem_pass.value ? space_rem_pass.value.trimStart() : ''; 
    })

function val_pass(){
	var reg_pass_s = document.getElementById("reg_pass_check").value;
	var reg_conf_pass_check = document.getElementById("reg_conf_pass_check").value;
	var upp=/(?=.*[A-Z])/;
	var low=/(?=.*[a-z])/;
	var digit=/(?=.*\d)/;
	var sp=/(?=.*[@#$!^%*?&])/;
	var pass_pattern = /^(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!^%*?&]{8,}$/;
	if(upp.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one uppercase";
		// document.getElementById('val_submit').disabled=false;
		return false;
	}
	else if(low.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one lower";
		// document.getElementById('val_submit').disabled=false;
		return false;
	}
	else if(digit.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one digit";
		// document.getElementById('val_submit').disabled=false;
		return false;
	}else if(sp.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast one spec";
		// document.getElementById('val_submit').disabled=false;
		return false;
	}else if(pass_pattern.test(reg_pass_s) == false) {
		document.getElementById("enter_pass").innerHTML = "alteast 8 cracters";
		// document.getElementById('val_submit').disabled=false;
		return false;
	}
	else{
		document.getElementById("enter_pass").innerHTML = "";
		return true;
	}
}

const space_rem_pass_conf = document.querySelector('#reg_conf_pass_check');
    space_rem_pass_conf.addEventListener('input', () => {
      space_rem_pass_conf.value = space_rem_pass_conf.value ? space_rem_pass_conf.value.trimStart() : ''; 
    })

function val_conf_pass(){
	var reg_pass = document.getElementById("reg_pass_check").value;
	var reg_conf_pass_check = document.getElementById("reg_conf_pass_check").value;
	if (reg_pass != reg_conf_pass_check) {
		document.getElementById("reg_pass_check").style.border = "";
		// document.getElementById('val_submit').disabled=true;
		return false;
	}
	else {
		document.getElementById("reg_pass_check").style.border = "none";
		document.getElementById("reg_pass_check").style.borderBottom = "2px solid #0174b9";
		// document.getElementById('val_submit').disabled=false;
		return true;
	}
}
function validatePassword(){
	var reg_pass = document.getElementById("reg_pass_check").value;
	var reg_conf_pass_check = document.getElementById("reg_conf_pass_check").value;
	if (reg_pass == reg_conf_pass_check || reg_conf_pass_check == "") {
		document.getElementById("enter_conf_pass").innerHTML = "";
		document.getElementById('val_submit').disabled=false;
		return false
	}
	else {
		document.getElementById("enter_conf_pass").innerHTML = "password does not match";
		document.getElementById('val_submit').disabled=true;
		return true;
	}
}
const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#reg_pass_check");

        togglePassword.addEventListener("click", function () {
            // toggle the type attribute
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            
            // toggle the icon
            this.classList.toggle("fa-eye-slash");
        });
        function move(e,prev,cur,next) {
  var length = document.getElementById(cur).value.length;
  var maxlength = document.getElementById(cur).getAttribute("maxlength");
  if(length == maxlength){
    if(next !== ""){
    document.getElementById(next).focus();
    }
  }
  if(e.key == 'Backspace'){
    if(prev !== ""){
    document.getElementById(prev).focus();
    }

  }

}
