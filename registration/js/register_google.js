const space_rem_email = document.querySelector('#reg_email_check');
    space_rem_email.addEventListener('input', () => {
      space_rem_email.value = space_rem_email.value ? space_rem_email.value.trimStart() : ''; 
    })

function val_email(){
	var email_err;
	var reg_email_s = document.getElementById("reg_email_check").value;
	var email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
	if(email_pattern.test(reg_email_s) == false){	
		document.getElementById("enter_email").innerHTML = "not valid";
		// document.getElementById('val_submit').disabled=true	;
		// return true;
		email_err=false;
	}
	else {
		document.getElementById("enter_email").innerHTML = "";
		// document.getElementById('val_submit').disabled=false;
		// return false;
		email_err=true;
	}
	return email_err;
}
const space_rem_pass = document.querySelector('#reg_pass_check');
    space_rem_pass.addEventListener('input', () => {
      space_rem_pass.value = space_rem_pass.value ? space_rem_pass.value.trimStart() : ''; 
    })
    
    const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#reg_pass_check");

        togglePassword.addEventListener("click", function () {
            // toggle the type attribute
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            
            // toggle the icon
            this.classList.toggle("fa-eye-slash");
        });
