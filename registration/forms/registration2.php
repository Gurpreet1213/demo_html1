<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Registration With Facebook</title>
	<link rel="stylesheet" type="text/css" href="../css/register.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>
<body>
	<?php include '../php/reg_otp.php'; ?>
	<div class="bg_image" style="background-image: url('../img/bg_image.png');">
	<div class="container d-flex p-4 ">
		<div class="col-md-4 m-auto " style="background-color: #009aee;
	opacity: 0.8;">
			<div class="row justify-content-center pt-3 text-white">
			<h3>Create an Account</h3>
			</div>
			<form id="reg_form" method="POST">
				<div class="form-group">
					<input type="text" class="input_dec form-control mt-3" name="reg_name" placeholder="Enter Name" id="reg_name_check"  maxlength="20" onkeyup="val_name()" onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)" onpaste="return false" required autocomplete="off">
					<p id="enter_name" class="text-danger"><?php echo $name_err;?></p>
					
				</div>
				<div class="form-group">
					<input type="email" class="input_dec form-control mt-3" maxlength="30" id="reg_email_check" name="reg_email" placeholder="E-mail" onkeyup="val_email()" onpaste="return false" required autocomplete="off" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
					<p id="enter_email" class="text-danger"><?php echo $email_err;?></p>
				</div>
				<div class="form-group">
					<input type="text" class="input_dec form-control mt-3" id="reg_mob_check"  minlength="5" maxlength="10" name="mob_no" placeholder="Mobile No." onkeyup="val_mobile()" onkeypress = "return  (event.charCode > 46 && event.charCode < 58)" onpaste="return false" required autocomplete="off" pattern="^[9876][0-9]{9}">
					<p id="enter_mob" class="text-danger"><?php echo $mob_err;?></p>
				</div>
				<div class="form-group position-relative">
					<i class="fas fa-eye position-absolute" id="togglePassword"></i>
					<input type="password" class="input_dec form-control mt-3" id="reg_pass_check" name="reg_pass" placeholder="Password" required maxlength="12" onkeyup="val_pass(); validatePassword();" onsubmit="validatePassword1()" onpaste="return false" autocomplete="off" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}$" title="plz enter an uppercase, a special cracter and a lowercase">
				</div>
				<p id="enter_pass" class="text-danger"><?php echo $pass_err;?></p>
				<div class="form-group">
					<input type="password" class="input_dec form-control mt-3" id="reg_conf_pass_check" name="reg_con_pass" onpaste="return false" placeholder="Confirm Password" onkeyup="val_conf_pass(); validatePassword();" onsubmit="validatePassword1()" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!^%*?&]{8,}$" title="plz enter an uppercase cracter, a special cracter and a lowercase cracter" required autocomplete="off">
					<p id="enter_conf_pass" class="text-danger"><?php echo $conf_pass_err;?></p>
				</div>
				<div class="row justify-content-center">
    	<div class="col-md-10 d-flex mt-3">
        <button class="button_login" id="val_submit" name="register" onclick="validatePassword();val_submit();">Submit</button>
      </div>
      <div class="container mt-3 d-inline-flex">
      	<div class="col-md-2">
      		<input #otpinput1 type="tel" class="input_otp w-100" onkeypress = "return  (event.charCode > 46 && event.charCode <= 57)" id="1st" maxlength="1" onkeyup= "move(event,'','1st','2nd')"/>
      		</div>
      		<div class="col-md-2">
      		<input #otpinput1 type="tel" class="input_otp w-100" onkeypress = "return  (event.charCode > 46 && event.charCode <= 57)" id="2nd" maxlength="1" onkeyup= "move(event,'1st','2nd','3rd')"/>
      		</div>
      		<div class="col-md-2">
      		<input #otpinput1 type="tel" class="input_otp w-100" onkeypress = "return  (event.charCode > 46 && event.charCode <= 57)" id="3rd" maxlength="1" onkeyup= "move(event,'2nd','3rd','4th')"/>
      		</div>
      		<div class="col-md-2">
      		 <input #otpinput1 type="tel" class="input_otp w-100" onkeypress = "return  (event.charCode > 46 && event.charCode <= 57)" id="4th" maxlength="1" onkeyup= "move(event,'3rd','4th','5th')"/>
      		</div>
      		<div class="col-md-2">
      		<input #otpinput1 type="tel" class="input_otp w-100" onkeypress = "return  (event.charCode > 46 && event.charCode <= 57)" id="5th" maxlength="1" onkeyup= "move(event,'4th','5th','6th')"/>
      		</div>
      		<div class="col-md-2">
      		<input #otpinput1 type="tel" class="input_otp w-100" onkeypress = "return  (event.charCode > 46 && event.charCode <= 57)" id="6th" maxlength="1" onkeyup= "move(event,'5th','6th','')"/>
      		</div>
      </div>
      <div class="col-md-12 mt-3">
      	<h6 style="text-align: center; color: #fff;">alredy have an account? <a href="login.html"><span style="color: #24e1ff;">sign-in</span></a> 
      		<br>
      		<span>or</span>
      	</h6>
      </div>
    </div>
    <div class="row mb-2 justify-content-end">
    <button class="sign-up-fb">
		<img class="fb_logo" src="../img/facebook.png">
		<a class="fb-text" href="https://www.facebook.com/" target="_blank">Login with facebook</a>
		</button>
    </div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="../js/registration_otp.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</body>
</html>