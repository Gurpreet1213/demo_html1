<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/login_form.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
	<title>Login with facebook</title>
</head>
<body>
<div class="bg_image" style="background-image: url('../img/bg_image.png');">
	<div class="cont container d-flex">
		<div class="container form_block col-md-4 shadow rounded">
			<div class="row justify-content-center mt-4 pt-3 text-white">
			<h3>Login Here</h3>
			</div>
			<form>
			<div class="form-group">
					<input type="email" class="input_dec form-control mt-3" id="reg_email_check" maxlength="25" name="reg_email" placeholder="E-mail" onkeyup="val_email()" onpaste="return false" required autocomplete="off" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
					<p id="enter_email" class="text-danger"></p>
				</div>
				<div class="form-group position-relative PB-3">
          <i class="fas fa-eye position-absolute" id="togglePassword"></i>
					<input type="password" class="input_dec form-control mt-3" id="reg_pass_check" name="pass" placeholder="Password" required maxlength="20"  onpaste="return false" autocomplete="off"> 
					<p id="enter_pass" class="text-danger"></p>
				</div>
  			<div class="row">
  				<div class="col-md-10 ml-5 mr-4 text-white">
					<a href="#"><h6>Forget Password?</h6></a>
      		</div>
  			</div>
  			<div class="row justify-content-center">
    	<div class="col-md-10 d-flex mt-4">
        <button class="button_login">Login</button>
      </div>
      
      <div class="col-md-12 mt-4 mb-4">
      	<h6 style="text-align: center; color: #fff;">no account? <a href="registration.html"><span style="color: #24e1ff;">sign-up</span></a>
      	</h6>
      </div>
    </div>
    <div class="row justify-content-end mb-4">
    <button class="sign-up-fb">
		<img class="fb_logo" src="../img/facebook.png">
		<a class="fb-text" href="https://www.facebook.com/" target="_blank">Login with facebook</a>
		</button>

    </div>
  		</form>
			</div>
		</div>

</div>
<script type="text/javascript" src="../js/register_fb.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</body>
</html>