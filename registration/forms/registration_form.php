<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../css/registeration_form.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" >
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <title>Registeration</title>
</head>
<body>
  <?php include '../php/reg.php'; ?>
  <?php echo $data_submit; ?>
  
  <div class="bg_image" style="background-image: url('../img/bg_image.png');">
    <div class="container login_form d-flex pt-5">
      <div class="form_block col-md-7 m-auto shadow rounded">
        <div class="row justify-content-center pt-4 pb-5 text-white">
          <h3>Create an Account</h3>
        </div>
        <form id="reg_form" method="POST">
          <div class="row px-3">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" id="reg_first_name_check"  maxlength="15" name="reg_first_name" placeholder="First Name" onkeyup="val_first_name()"  pattern="^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$" onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)" onpaste="return false" required autocomplete="off" >
                <p id="enter_first_name" class="text-danger"><?php echo $name_err;?></p>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="reg_last_name_check"  maxlength="10" name="reg_last_name" placeholder="Last Name" onkeyup="val_last_name()" pattern="^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$" onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)" onpaste="return false" required autocomplete="off" >
                <p id="enter_last_name" class="text-danger"><?php echo $l_name_err;?></p>
              </div>
              <div class="form-group">
               <input type="email" class="form-control" id="reg_email_check" maxlength="25" name="reg_email" placeholder="E-mail" onkeyup="val_email()" onpaste="return false" required autocomplete="off" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
               <p id="enter_email" class="text-danger"><?php echo $email_err;?></p>
             </div>
             <div class="form-group">
              <input type="text" class="form-control" id="reg_mob_check"  minlength="5" maxlength="10" name="mob_no" placeholder="Mobile No." onkeyup="val_mobile()" onkeypress = "return  (event.charCode > 46 && event.charCode < 58)" onpaste="return false" required autocomplete="off" pattern="^[9876][0-9]{9}">
              <p id="enter_mob" class="text-danger"> <?php echo $mob_err;?></p>
            </div>
            <div class="form-group position-relative PB-3">
              <i class="fas fa-eye position-absolute" id="togglePassword"></i>
              <input type="password" class="form-control" id="reg_pass_check" name="pass" placeholder="Password" required maxlength="20" onkeyup="val_pass(); validatePassword();" onpaste="return false" autocomplete="off" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}$" title="plz enter an uppercase, a special cracter and a lowercase">  
            </div>
            <p id="enter_pass" class="text-danger"><?php echo $pass_err;?></p>
            <div class="form-group">
              <input type="password" class="form-control" id="reg_conf_pass_check" name="con_pass" onpaste="return false" placeholder="Confirm Password" onkeyup="val_conf_pass(); validatePassword();" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!^%*?&]{8,}$" title="plz enter an uppercase cracter, a special cracter and a lowercase cracter" required autocomplete="off">
              <p id="enter_conf_pass" class="text-danger"><?php echo $conf_pass_err;?></p>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <select class="select_dec dropdown form-control w-100" name="sel_gen" required="required" autocomplete="off">
                <option class="dropdown-item" selected disabled value="">Select Gender</option>
                <option class="dropdown-item" value="male">Male</option>
                <option class="dropdown-item" value="female">Female</option>
                <option class="dropdown-item" value="other">Other</option>
              </select>
              <p class="text-danger"><?php echo $gen_err;?></p>
            </div>
            <div class="form-group">                    
              <textarea name="addr" class="form-control" id="reg_addr_check" onpaste="return false" rows="1" cols="50" maxlength="50"placeholder="Address" onkeyup="val_addr()" required="required" autocomplete="off"></textarea>
              <p class="text-danger"><?php echo $add_err;?></p>
            </div>
            <div class="form-group">
              <input type="text" name="pin_code" class="form-control" id="reg_pin_check" maxlength="6" placeholder="Pin Code" onkeyup="val_pin()" onfocusout="get_pin()" onkeypress = "return (event.charCode > 46 && event.charCode < 58)"onpaste="return false" required="required" autocomplete="off" pattern="^[1-9].[0-9]{4,6}">
              <p id="enter_pin" class="text-danger"><?php echo $pin_err;?></p>
            </div>
            <div class="form-group"> 
              <input type="text" name="city" class="form-control" id="reg_city_check" onpaste="return false" onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)" placeholder="City" maxlength="20" onkeyup="val_city()" autocomplete="off" required="required" readonly>
              <p id="enter_city" class="text-danger"></p>
            </div>
            <div class="form-group"> 
             <input type="text" name="state" class="form-control" placeholder="State" maxlength="20" onkeyup="val_state()" onpaste="return false" onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)" id="reg_state_check" autocomplete="off" required="required" readonly>
             <p id="enter_state" class="text-danger"></p>
           </div>
           <div class="form-group">
            <input type="text" name="country" class="form-control" onpaste="return false" placeholder="Country" maxlength="20" id="reg_country_check" onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)" onkeyup="val_country()" autocomplete="off" required="required" pattern="^[A-Za-z\s]+$" readonly>
            <p id="enter_country" class="text-danger"></p>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-5 mt-3">
          <button class="button_login" type="submit" name="register" id="val_submit" onclick="validatePassword();get_pin();">Submit</button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 mt-4">
          <h6 style="text-align: center; color: #fff;">alredy have an account? <a href="login_google.html"><span style="color: #24e1ff;">sign-in</span></a>
          </h6>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script type="text/javascript" src="../js/registration.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>